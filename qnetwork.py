from torch import nn
from torch import optim
import torch
import gym
from collections import deque
import numpy as np
import random
from gym.wrappers.monitoring.video_recorder import VideoRecorder
from premier_agent import plotLearningCurve

MIN_REPLAY_SIZE = 1000
EPSILON_START = 1.0
EPSILON_END = 0.02
EPSILON_DECAY = 10000
TARGET_UPDATE_FREQ = 1000
EPISODES = 50000

GAMMA = 0.99

BATCH_SIZE = 32
BUFFER_SIZE = 100000

scores_avr = []
x = []


def save_model(model):
    torch.save(model.state_dict(), 'model_weights_DQN.pth')


def load_model(env):
    model = Net(env)
    model.load_state_dict(torch.load('model_weights_DQN.pth'))
    model.eval()
    return model


class Net(nn.Module):
    def __init__(self, env):
        super().__init__()

        in_features = int(np.prod(env.observation_space.shape))

        self.net = nn.Sequential(
            nn.Linear(in_features, 64),
            nn.Tanh(),
            nn.Linear(64, env.action_space.n)
        )
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.to(self.device)

    def forward(self, x):
        return self.net(x)

    def act(self, obs):
        obs_t = torch.as_tensor(obs, dtype=torch.float32)
        q_values = self(obs_t.unsqueeze(0))

        max_q_index = torch.argmax(q_values, dim=1)[0]
        action = max_q_index.detach().item()

        return action


env = gym.make('CartPole-v1')
env.seed(0)
video_recorder = None
video_recorder = VideoRecorder(env, "resultats_dqn/video_DQN.mp4", enabled=True)

replay_buffer = deque(maxlen=BUFFER_SIZE)
rew_buffer = deque([0.0], maxlen=100)

episode_reward = 0.0

policy_net = Net(env)
target_net = Net(env)

target_net.load_state_dict(policy_net.state_dict())

optimizer = optim.Adam(policy_net.parameters(), lr=5e-4)

# initialize Replay Buffer
obs = env.reset()
for _ in range(MIN_REPLAY_SIZE):
    action = env.action_space.sample()

    new_obs, rew, done, _ = env.step(action)
    transition = (obs, action, rew, done, new_obs)
    replay_buffer.append(transition)
    obs = new_obs

    if done:
        obs = env.reset()

# Main trainig loop
obs = env.reset()

for step in range(EPISODES):

    # epsilon_greedy
    epsilon = np.interp(step, [0, EPSILON_DECAY], [EPSILON_START, EPSILON_END])

    rnd_sample = random.random()
    if rnd_sample <= epsilon:
        action = env.action_space.sample()
    else:
        action = policy_net.act(obs)

    new_obs, rew, done, _ = env.step(action)
    transition = (obs, action, rew, done, new_obs)
    replay_buffer.append(transition)
    obs = new_obs

    episode_reward += rew

    if done:
        obs = env.reset()

        rew_buffer.append(episode_reward)
        episode_reward = 0.0

    # Start Gradient Step
    transitions = random.sample(replay_buffer, BATCH_SIZE)

    # get the experiences
    obses = np.asanyarray([t[0] for t in transitions])
    actions = np.asanyarray([t[1] for t in transitions])
    rews = np.asanyarray([t[2] for t in transitions])
    dones = np.asanyarray([t[3] for t in transitions])
    new_obses = np.asanyarray([t[4] for t in transitions])

    # from arrays to tensors
    obses_t = torch.as_tensor(obses, dtype=torch.float32)
    actions_t = torch.as_tensor(actions, dtype=torch.int64).unsqueeze(-1)
    rews_t = torch.as_tensor(rews, dtype=torch.float32).unsqueeze(-1)
    dones_t = torch.as_tensor(dones, dtype=torch.float32).unsqueeze(-1)
    new_obses_t = torch.as_tensor(new_obses, dtype=torch.float32)

    # compute Targets 
    target_q_values = target_net(new_obses_t)
    max_target_q_values = target_q_values.max(dim=1, keepdim=True)[0]

    targets = rews_t + GAMMA * (1 - dones_t) * max_target_q_values

    # compute loss
    q_values = policy_net(obses_t)
    action_q_values = torch.gather(input=q_values, dim=1, index=actions_t)
    loss = nn.functional.smooth_l1_loss(action_q_values, targets)

    # Gradient Descent
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    # update Target Network
    if step % TARGET_UPDATE_FREQ == 0:
        target_net.load_state_dict(policy_net.state_dict())

    # Logging (Episodes)
    if step % 100 == 0:
        print()
        print('Step', step)
        print('Avg Rew', np.mean(rew_buffer))
        scores_avr.append(np.mean(rew_buffer))
        x.append(step / 100)

filename = 'resultats_dqn/DQN.png'
plotLearningCurve(x, scores_avr, filename)

# playing 5 games after solving
play = 0
obs = env.reset()
while play < 5:
    action = policy_net.act(obs)
    obs, _, done, _ = env.step(action)
    env.render()
    video_recorder.capture_frame()
    if done:
        env.reset()
        play = play + 1

print("Saved video.")
video_recorder.close()
video_recorder.enabled = False
env.close()

# save model
save_model(policy_net)
