# Bio Inpired Machine Learning
## Projet DQN

Dans ce projet, on trouve 2 fichiers ".py" :

### "premier_Agent.py" : 
Ce fichier contient le code de la 1ère partie où on a mis en place l'agent aléatoire dans 
l'environnement CartPole-v1.

### "q_network.py" :
Ce fichier contient le code où on a implémenter du Deep Q_Network dans l'environnement CartPole-v1.
Les paramètres utilisées sont les suivants :
<ul>
            <li>EPSILON_START = 1.0</li>
            <li>EPSILON_END  = 0.02</li>
            <li>EPSILON_DECAY = 10000</li>
            <li>MIN_REPLAY_SIZE = 1000</li>
            <li>TARGET_UPDATE_FREQ = 1000</li>
            <li>EPISODES = 50000</li>
            <li>BATCH_SIZE = 32</li>
            <li>BUFFER_SIZE = 100000</li>
</ul>


Le fichier "Rapport_Choukri_Dafer.pdf" contient le rapport de ce projet.
Ce travail est réalisé par :
<ul>
    <li>CHOUKRI Ayoub P2213664</li>
    <li>DAFER Yahya P2212924</li>
</ul>
