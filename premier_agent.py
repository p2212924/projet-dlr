import gym
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import matplotlib.pyplot as plt


def plotLearningCurve(x, scores, filename, lines=None):
    fig = plt.figure()
    ax = fig.add_subplot(111, label="1")
    ax.plot(x, scores)
    ax.set_xlabel("Episode")
    ax.set_ylabel("Recompense")
    ax.tick_params(axis='x')
    ax.tick_params(axis='y')
    if lines is not None:
        for line in lines:
            plt.axvline(x=line)

    plt.savefig(filename)


# environnement initialisation
env = gym.make('CartPole-v1')
env.seed(0)

# video recording
video_recorder = None
video_recorder = VideoRecorder(env, 'resultats_random/video_Premier_Agent.mp4', enabled=True)

etapes = []
all_rewards = []

# starting episodes
for i_episode in range(50):
    state = env.reset()
    inter_num = 0
    rewards = 0
    while True:
        env.render()
        video_recorder.capture_frame()
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)
        inter_num = inter_num + 1
        rewards = rewards + reward
        rewards = rewards + reward
        if done:
            break
    print("Episode : " + str(i_episode) + " ; nb interactions : " + str(inter_num) + " ; recompenses : " + str(
        rewards))
    all_rewards.append(rewards)
    etapes.append(i_episode)

print("Saved video.")
video_recorder.close()
video_recorder.enabled = False
filename = 'resultats_random/Premier_Agent.png'
plotLearningCurve(etapes, all_rewards, filename)

env.env.close()

